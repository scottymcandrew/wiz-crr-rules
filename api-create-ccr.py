"""
Python 3.6+
pip(3) install requests
"""
import base64
import json
import requests
import os

# Standard headers
HEADERS_AUTH = {"Content-Type": "application/x-www-form-urlencoded"}
HEADERS = {"Content-Type": "application/json"}

# Remember to set Wiz Service Account details as environment variables
CLIENT_ID = os.environ.get("WIZ_API_TF_PROVIDER_CLIENT_ID")
CLIENT_SECRET = os.environ.get("WIZ_API_TF_PROVIDER_CLIENT_SECRET")

# Uncomment the following section to define the proxies in your environment,
#   if necessary:
# http_proxy  = "http://"+user+":"+passw+"@x.x.x.x:abcd"
# https_proxy = "https://"+user+":"+passw+"@y.y.y.y:abcd"
# proxyDict = {
#     "http"  : http_proxy,
#     "https" : https_proxy
# }

# The GraphQL query that defines which data you wish to fetch.
QUERY = """
    mutation CreateCloudConfigRule($input: CreateCloudConfigurationRuleInput!) {
      createCloudConfigurationRule(input: $input) {
        rule {
          id
        }
      }
    }
"""

# The variables sent along with the above query
VARIABLES = {
  "input": {
    "name": "scott-mcandrew-IMDSv2-check",
    "description": "This rule checks whether the EC2 instance only allows the use of the Instance Metadata Service Version 2 (IMDSv2).  \nThis rule fails if `HttpTokens` is set to `optional` or `MetadataOptions.State` is `pending`.  \nThe Instance Metadata Service allows EC2s to query a private link address for their own metadata and data input by the user.  \nThis has many benefits, including better credential safety - since the need to hard-code credentials, or distribute sensitive credentials to instances is eliminated by the use of this service.  \nThe first version of this service was prone to several forms of attack, which allowed attackers access to instances' sensitive metadata and credentials.  \nFor this reason, AWS developed an enhanced version of this service - helping protect instances from forms of attack previously made possible.  \nIt is recommended to only allow the use of IMDSv2 on EC2 instances, to help protect them from attacks exploiting the older version of the service.\n",
    "opaPolicy": "package wiz\n\ndefault result = \"pass\"\n\nincompliantIMDS {\n\tlower(input.MetadataOptions.HttpTokens) == \"optional\"\n}{\n\tlower(input.MetadataOptions.State) == \"pending\"\n}\n\nresult = \"fail\" {\n\tincompliantIMDS\n}\n\ncurrentConfiguration := \"'HttpTokens': 'optional'\"\nexpectedConfiguration := \"'HttpTokens' should be set to 'required'\"\n",
    "targetNativeTypes": [
      "virtualMachine"
    ],
    "severity": "HIGH",
    "remediationInstructions": "Perform the following command to configure EC2 to only allow the use of IMDSv2 via AWS CLI:  \n```  \naws ec2 modify-instance-metadata-options --instance-id {{instanceId}} --http-tokens required --http-endpoint enabled  \n```\n>**Note**  \n>It is recommended to make sure the EC2's software supports IMDSv2 prior to performing this command.  \n>Follow [this link](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-cfg-ec2-imds.html) for things to take into consideration before switching over to using only IMDSv2, and [this link](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-metadata-transition-to-version-2.html) for tools and recommendations for performing the transition.",
    "securitySubCategories": [
      "wsct-id-7659",
      "783add90-1d74-4868-b5d2-2d9ff8a55d4f",
      "80d9d0c8-d36e-418d-99a2-e1f06612534f",
      "716863d4-8289-4bf5-89d2-46eafedc7e8f",
      "67a28174-bdcc-4e5b-8fe4-7c528f24b8b6",
      "d785efc6-2970-4364-81a3-3708247f3551",
      "0f9e1659-0cdb-4f97-a1e4-f1cfd59971e0",
      "a86807d9-e1e3-423b-a485-5b6588ee5248",
      "d2336db4-4ee9-4337-b606-83b5a954ea6d",
      "d0771447-1e77-402f-8475-dbc506151234",
      "63aa920b-8e7b-4247-af5f-ec3e9030db64",
      "43f837c7-7c4f-4064-b1cd-4c834405081f",
      "wsct-id-9619",
      "3c021e04-23e3-49b8-a32d-370bb70c94bf",
      "e664fd67-2b9a-4a56-a35a-bdc576922617",
      "82ddd0fa-3137-4b6e-90de-ad602053da9e",
      "bdfbe96e-dd02-4301-81c1-e65e40520bad",
      "wsct-id-7",
      "wsct-id-3759",
      "wsct-id-10792",
      "ca1521ae-2016-4423-bd6f-5a5a1bf9590e",
      "e798e2a3-1299-4ab4-a753-daafff2aafcd",
      "wsct-id-11277",
      "wsct-id-13782"
    ],
    "scopeAccountIds": [],
    "functionAsControl": True,
    "iacMatchers": [
      {
        "regoCode": "package wiz\n\nimport data.generic.common as commonLib\nimport data.generic.terraform as terraLib\n\ncheckHttpToken(resource) {\n\tnot commonLib.valid_key(resource, \"metadata_options\")\n}{\n\tnot commonLib.valid_key(terraLib.getValueArrayOrObject(resource.metadata_options), \"http_tokens\")\n}{\n\tterraLib.getValueArrayOrObject(resource.metadata_options).http_tokens == \"optional\"\n}\n\t\n\nWizPolicy[result] {\n\tdoc := input.document[i]\n\tresource := doc.resource.aws_instance[name]\n\tcheckHttpToken(resource)\n\t\t\n\tresult := {\n\t\t\"documentId\": doc.id,\n\t\t\"searchKey\": sprintf(\"aws_instance[%s]\", [name]),\n\t\t\"keyExpectedValue\": \"'metadata_options.http_tokens' field should not be set to 'optional'\",\n\t\t\"keyActualValue\": \"'metadata_options.http_tokens' field is set to 'optional'\",\n\t\t\"resourceTags\": object.get(resource, \"tags\", {}),\n\t}\n}\n",
        "type": "TERRAFORM"
      }
    ],
    "tags": []
  }
}


def query_wiz_api(query, variables, dc):
    """Query Wiz API for the given query data schema"""

    data = {"variables": variables, "query": query}

    try:
        # Uncomment the next first line and comment the line after that
        # to run behind proxies
        # result = requests.post(url=f"https://api.{dc}.app.wiz.io/graphql",
        #                        json=data, headers=HEADERS, proxies=proxyDict, timeout=180)
        result = requests.post(url=f"https://api.{dc}.app.wiz.io/graphql",
                               json=data, headers=HEADERS, timeout=180)

    except requests.exceptions.HTTPError as e:
        print(f"<p>Wiz-API-Error (4xx/5xx): {str(e)}</p>")
        return e

    except requests.exceptions.ConnectionError as e:
        print(f"<p>Network problem (DNS failure, refused connection, etc): {str(e)}</p>")
        return e

    except requests.exceptions.Timeout as e:
        print(f"<p>Request timed out: {str(e)}</p>")
        return e

    return result.json()


def request_wiz_api_token(client_id, client_secret):
    """Retrieve an OAuth access token to be used against Wiz API"""

    auth_payload = {
      'grant_type': 'client_credentials',
      'audience': 'wiz-api',
      'client_id': client_id,
      'client_secret': client_secret
    }
    try:
        # Uncomment the next first line and comment the line after that
        # to run behind proxies
        # response = requests.post(url="https://auth.app.wiz.io/oauth/token",
        #                         headers=HEADERS_AUTH, data=auth_payload,
        #                         proxies=proxyDict, timeout=180)
        response = requests.post(url="https://auth.app.wiz.io/oauth/token",
                                headers=HEADERS_AUTH, data=auth_payload, timeout=180)

    except requests.exceptions.HTTPError as e:
        print(f"<p>Error authenticating to Wiz (4xx/5xx): {str(e)}</p>")
        return e

    except requests.exceptions.ConnectionError as e:
        print(f"<p>Network problem (DNS failure, refused connection, etc): {str(e)}</p>")
        return e

    except requests.exceptions.Timeout as e:
        print(f"<p>Request timed out: {str(e)}</p>")
        return e

    try:
        response_json = response.json()
        token = response_json.get('access_token')
        if not token:
            message = f"Could not retrieve token from Wiz: {response_json.get('message')}"
            raise ValueError(message)
    except ValueError as exception:
        message = f"Could not parse API response {exception}. Check Service Account details " \
                    "and variables"
        raise ValueError(message) from exception

    response_json_decoded = json.loads(
        base64.standard_b64decode(pad_base64(token.split(".")[1]))
    )

    response_json_decoded = json.loads(
        base64.standard_b64decode(pad_base64(token.split(".")[1]))
    )
    dc = response_json_decoded["dc"]

    return token, dc


def pad_base64(data):
    """Makes sure base64 data is padded"""
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += "=" * (4 - missing_padding)
    return data


def main():
    """Main function"""

    print("Getting token.")
    token, dc = request_wiz_api_token(CLIENT_ID, CLIENT_SECRET)
    HEADERS["Authorization"] = "Bearer " + token

    result = query_wiz_api(QUERY, VARIABLES, dc)
    print(result)  # your data is here!




if __name__ == '__main__':
    main()
