resource "wiz_security_framework" "_0002-scott-mcandrew-tf-framework" {
  name        = "terraform-test-security-framework1"
  description = "test description"
  enabled     = true
  category {
    name        = "AM Asset Management"
    description = "test am description"
    sub_category {
      title = "AM-1 Track asset inventory and their risks"
      controls = ["wc-id-528", "wc-id-942"]
      host_configuration_rules = ["fff6fd5e-1ec3-4119-a3b6-7f36a9e94c50", "fff6bcdd-664c-4f17-bb66-d310de5131c9"]

    }
  }
  category {
    name        = "test category 2"
    description = "test description 2"
    sub_category {
      title       = "test subcategory"
      description = "bad stuff now"
      cloud_configuration_rules = ["ffe428e3-f146-431a-b97b-cb8f842a9465", "ff858ac7-0c20-44ee-aac6-01118f959fce"]
    }
    sub_category {
      title       = "test subcategory 2"
      description = "bad stuff could happen"
    }
  }
}