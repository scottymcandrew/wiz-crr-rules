provider "azurerm" {
  features {

  }
}

resource "azurerm_mssql_server_extended_auditing_policy" "eap" {
  server_id = azurerm_mssql_server.sqls.id
}

resource "azurerm_mssql_server_security_alert_policy" "sap" {
  resource_group_name = var.resource_group_name
  server_name = azurerm_mssql_server.sqls.name
  state = "Enabled"
}

resource "azurerm_mssql_server" "sqls" {
  resource_group_name = var.resource_group_name
  version = "12.0"
  location = var.location
  name = "sqlsvr01010101010101"
  administrator_login_password = "P@ssw0rd"
  administrator_login = "@3414!@%ADFA"
}

resource "azurerm_resource_group" "vuln_rg" {
  name     = "vulnerable-resources"
  location = "East US"
}

resource "azurerm_virtual_network" "vuln_vnet" {
  name                = "vulnerableVNet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.vuln_rg.location
  resource_group_name = azurerm_resource_group.vuln_rg.name
}

resource "azurerm_subnet" "vuln_subnet" {
  name                 = "vulnerableSubnet"
  resource_group_name  = azurerm_resource_group.vuln_rg.name
  virtual_network_name = azurerm_virtual_network.vuln_vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_security_group" "vuln_nsg" {
  name                = "vulnerableNSG"
  location            = azurerm_resource_group.vuln_rg.location
  resource_group_name = azurerm_resource_group.vuln_rg.name

  security_rule {
    name                       = "RDP-Access"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "0.0.0.0/0"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "vuln_subnet_nsg_assoc" {
  subnet_id                 = azurerm_subnet.vuln_subnet.id
  network_security_group_id = azurerm_network_security_group.vuln_nsg.id
}

resource "azurerm_public_ip" "vuln_vm_ip" {
  name                = "vulnerableVMIP"
  location            = azurerm_resource_group.vuln_rg.location
  resource_group_name = azurerm_resource_group.vuln_rg.name
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "vuln_vm_nic" {
  name                = "vulnerableVMNIC"
  location            = azurerm_resource_group.vuln_rg.location
  resource_group_name = azurerm_resource_group.vuln_rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.vuln_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vuln_vm_ip.id
  }
}

resource "azurerm_virtual_machine" "vuln_vm" {
  name                  = "vulnerableVM"
  location              = azurerm_resource_group.vuln_rg.location
  resource_group_name   = azurerm_resource_group.vuln_rg.name
  network_interface_ids = [azurerm_network_interface.vuln_vm_nic.id]
  vm_size               = "Standard_F1"

  storage_os_disk {
    name              = "vulnerableOSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2012-R2-Datacenter"  # Intentionally outdated version for vulnerability
    version   = "latest"
  }

  os_profile {
    computer_name  = "vulnerableVM"
    admin_username = "adminuser"
    admin_password = "ReplaceWithSecurePassword!"
  }

  os_profile_windows_config {
    enable_automatic_updates = false  # Keeping the system outdated
  }
}
