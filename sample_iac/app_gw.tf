resource "azurerm_resource_group" "appgw_rg" {
  name     = "appgw-resources"
  location = "East US"
}

resource "azurerm_public_ip" "appgw_public_ip" {
  name                = "appgwPublicIP"
  location            = azurerm_resource_group.appgw_rg.location
  resource_group_name = azurerm_resource_group.appgw_rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_application_gateway" "appgw" {
  name                = "example-appgateway"
  location            = azurerm_resource_group.appgw_rg.location
  resource_group_name = azurerm_resource_group.appgw_rg.name

  sku {
    name     = "Standard_v2"
    tier     = "Standard_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "appgwIpConfig"
    subnet_id = "<Your-Subnet-ID-Here>"
  }

  frontend_port {
    name = "appgwFrontendPort"
    port = 80
  }

  frontend_ip_configuration {
    name                 = "appgwFrontendIPConfig"
    public_ip_address_id = azurerm_public_ip.appgw_public_ip.id
  }

  backend_address_pool {
    name = "appgwBackendPool"
  }

  backend_http_settings {
    name                  = "appgwBackendHttpSettings"
    cookie_based_affinity = "Disabled"
    path                  = "/"
    port                  = 80
    protocol              = "Http"
  }

  http_listener {
    name                           = "appgwHttpListener"
    frontend_ip_configuration_name = "appgwFrontendIPConfig"
    frontend_port_name             = "appgwFrontendPort"
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = "appgwRule1"
    rule_type                  = "Basic"
    http_listener_name         = "appgwHttpListener"
    backend_address_pool_name  = "appgwBackendPool"
    backend_http_settings_name = "appgwBackendHttpSettings"
  }
}
