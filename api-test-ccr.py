"""
Python 3.6+
pip(3) install requests
"""
import base64
import json
import requests
import os

# Standard headers
HEADERS_AUTH = {"Content-Type": "application/x-www-form-urlencoded"}
HEADERS = {"Content-Type": "application/json"}

# Remember to set Wiz Service Account details as environment variables
CLIENT_ID = os.environ.get("WIZ_API_TF_PROVIDER_CLIENT_ID")
CLIENT_SECRET = os.environ.get("WIZ_API_TF_PROVIDER_CLIENT_SECRET")

# Uncomment the following section to define the proxies in your environment,
#   if necessary:
# http_proxy  = "http://"+user+":"+passw+"@x.x.x.x:abcd"
# https_proxy = "https://"+user+":"+passw+"@y.y.y.y:abcd"
# proxyDict = {
#     "http"  : http_proxy,
#     "https" : https_proxy
# }

# The GraphQL query that defines which data you wish to fetch.
QUERY = """
    query RunIaCTestWithFile($rule: String!, $IaCFileContent: String!, $type: CloudConfigurationRuleMatcherType!) {
      cloudConfigurationRuleIaCTest(
        rule: $rule
        IaCFileContent: $IaCFileContent
        type: $type
      ) {
        result
        output
        evidence {
          current
          expected
          path
        }
      }
    }
"""

# Open the converted IaC file to evaluate against the proposed Rego Code
with open('./converted-iac.json', 'r') as converted_iac_file:
    iac_file_contents = converted_iac_file.read()

# Open the proposed Rego file
with open('./Cloud_Configuration_Rules/CCR-00001/terraform/query.rego', 'r') as rego_file:
    rego_file_contents = rego_file.read()

# The variables sent along with the above query
VARIABLES = {
#   "IaCFileContent": "provider \"aws\" {\n  region = \"us-east-1\"\n}\n\nresource \"aws_instance\" \"example\" {\n  ami           = \"ami-123456\" # Please replace with a valid AMI for your region\n  instance_type = \"t2.micro\"\n\n  metadata_options {\n    http_endpoint               = \"enabled\"\n    http_tokens                 = \"optional\"\n    http_put_response_hop_limit = 1\n    instance_metadata_tags      = \"disabled\"\n  }\n}\n",
  "IaCFileContent": iac_file_contents,
#  "rule": "package wiz\n\nimport data.generic.common as commonLib\nimport data.generic.terraform as terraLib\n\ncheckHttpToken(resource) {\n\tnot commonLib.valid_key(resource, \"metadata_options\")\n}{\n\tnot commonLib.valid_key(terraLib.getValueArrayOrObject(resource.metadata_options), \"http_tokens\")\n}{\n\tterraLib.getValueArrayOrObject(resource.metadata_options).http_tokens == \"optional\"\n}\n\t\n\nWizPolicy[result] {\n\tdoc := input.document[i]\n\tresource := doc.resource.aws_instance[name]\n\tcheckHttpToken(resource)\n\t\t\n\tresult := {\n\t\t\"documentId\": doc.id,\n\t\t\"searchKey\": sprintf(\"aws_instance[%s]\", [name]),\n\t\t\"keyExpectedValue\": \"'metadata_options.http_tokens' field should not be set to 'optional'\",\n\t\t\"keyActualValue\": \"'metadata_options.http_tokens' field is set to 'optional'\",\n\t\t\"resourceTags\": object.get(resource, \"tags\", {}),\n\t}\n}\n",
  "rule": rego_file_contents,
  "type": "TERRAFORM"
}


def query_wiz_api(query, variables, dc):
    """Query Wiz API for the given query data schema"""

    data = {"variables": variables, "query": query}

    try:
        # Uncomment the next first line and comment the line after that
        # to run behind proxies
        # result = requests.post(url=f"https://api.{dc}.app.wiz.io/graphql",
        #                        json=data, headers=HEADERS, proxies=proxyDict, timeout=180)
        result = requests.post(url=f"https://api.{dc}.app.wiz.io/graphql",
                               json=data, headers=HEADERS, timeout=180)

    except requests.exceptions.HTTPError as e:
        print(f"<p>Wiz-API-Error (4xx/5xx): {str(e)}</p>")
        return e

    except requests.exceptions.ConnectionError as e:
        print(f"<p>Network problem (DNS failure, refused connection, etc): {str(e)}</p>")
        return e

    except requests.exceptions.Timeout as e:
        print(f"<p>Request timed out: {str(e)}</p>")
        return e

    return result.json()


def request_wiz_api_token(client_id, client_secret):
    """Retrieve an OAuth access token to be used against Wiz API"""

    auth_payload = {
      'grant_type': 'client_credentials',
      'audience': 'wiz-api',
      'client_id': client_id,
      'client_secret': client_secret
    }
    try:
        # Uncomment the next first line and comment the line after that
        # to run behind proxies
        # response = requests.post(url="https://auth.app.wiz.io/oauth/token",
        #                         headers=HEADERS_AUTH, data=auth_payload,
        #                         proxies=proxyDict, timeout=180)
        response = requests.post(url="https://auth.app.wiz.io/oauth/token",
                                headers=HEADERS_AUTH, data=auth_payload, timeout=180)

    except requests.exceptions.HTTPError as e:
        print(f"<p>Error authenticating to Wiz (4xx/5xx): {str(e)}</p>")
        return e

    except requests.exceptions.ConnectionError as e:
        print(f"<p>Network problem (DNS failure, refused connection, etc): {str(e)}</p>")
        return e

    except requests.exceptions.Timeout as e:
        print(f"<p>Request timed out: {str(e)}</p>")
        return e

    try:
        response_json = response.json()
        token = response_json.get('access_token')
        if not token:
            message = f"Could not retrieve token from Wiz: {response_json.get('message')}"
            raise ValueError(message)
    except ValueError as exception:
        message = f"Could not parse API response {exception}. Check Service Account details " \
                    "and variables"
        raise ValueError(message) from exception

    response_json_decoded = json.loads(
        base64.standard_b64decode(pad_base64(token.split(".")[1]))
    )

    response_json_decoded = json.loads(
        base64.standard_b64decode(pad_base64(token.split(".")[1]))
    )
    dc = response_json_decoded["dc"]

    return token, dc


def pad_base64(data):
    """Makes sure base64 data is padded"""
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += "=" * (4 - missing_padding)
    return data


def main():
    """Main function"""

    print("Getting token.")
    token, dc = request_wiz_api_token(CLIENT_ID, CLIENT_SECRET)
    HEADERS["Authorization"] = "Bearer " + token

    result = query_wiz_api(QUERY, VARIABLES, dc)
    print(result)  # your data is here!




if __name__ == '__main__':
    main()