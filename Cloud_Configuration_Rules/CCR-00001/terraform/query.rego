package wiz

import data.generic.common as common_lib
import data.generic.terraform as terraLib

# Check for resources missing specific tag key and value
WizPolicy[result] {
	resource := input.document[i].resource[type][name]
	tags := object.get(resource, "tags", {})

	tag_key_pattern := "^(highlyrestricted|restricted|corporate|public)$"
	tag_value_pattern := "TBC"

	not has_matching_tag(tags, tag_key_pattern, tag_value_pattern)

	result := {
		"documentId": input.document[i].id,
		"resourceName": terraLib.get_resource_name(resource, name),
		"searchKey": sprintf("%s[%s].tags", [type, name]),
		"issueType": "MissingAttribute",
		"keyExpectedValue": sprintf("Resource should have a tag key matching '%s' with the value '%s'", [tag_key_pattern, tag_value_pattern]),
		"keyActualValue": "Resource is missing the required tag key and value",
		"searchLine": common_lib.build_search_line(["resource", type, name, "tags"], []),
		"resourceTags": tags,
	}
}

# Helper function to check if tags contain matching key and value
has_matching_tag(tags, tag_key_pattern, tag_value_pattern) {
	some tag_key
	re_match(tag_key_pattern, tag_key)
	tags[tag_key] == tag_value_pattern
}